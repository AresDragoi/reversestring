﻿using System;

namespace sec
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = Console.ReadLine();
            
            System.Console.WriteLine(Reverse(input));
        }

        static string Reverse(string input)
        {
            char[] array = input.ToCharArray();
            Array.Reverse(array);
            return new string(array);
        }
    }
}
